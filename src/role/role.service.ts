import { Injectable } from '@nestjs/common';
import {
  Role
} from './types/Role.type';

import {
  User
} from '../user/types'

import { ORMService } from 'src/orm/orm.service';

@Injectable()
export class RoleService {
  readonly className = this.constructor.name;

  constructor(
    private readonly ormService: ORMService
  ) {}

  async findAll(): Promise<Role[]> {
      return this.ormService.getRoles();
  }

  async usersByRole(roleId: number): Promise<User[]> {
    const users = this.ormService.usersByRole(roleId);

    return users.map(({ roles, ...all }) => all);
  }

  async rolesByUser(userId: number): Promise<Role[]> {
    return this.ormService.rolesByUser(userId);
  }
}