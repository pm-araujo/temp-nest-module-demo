import { Resolver, Args, Query } from '@nestjs/graphql';

import { RoleService } from './role.service';

import { Role } from './types/Role.type';

import { User } from '../user/types/User.type';

@Resolver(of => Role)
export class RoleResolver {
  constructor(private readonly roleService: RoleService) {}

  @Query(returns => [Role])
  roles() {
    return this.roleService.findAll();
  }

  @Query(returns => [User])
  usersByRole(@Args('roleId') roleId: number) {
    return this.roleService.usersByRole(roleId);
  }
}
