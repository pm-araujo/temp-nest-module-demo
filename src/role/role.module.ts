import { Module } from "@nestjs/common";

import { ORMModule } from "src/orm/orm.module";
import { RoleService } from './role.service';
import { RoleResolver } from "./role.resolver";

@Module({
  imports: [
    ORMModule
  ],
  providers: [
    RoleService,
    RoleResolver
  ],
  exports: [
    RoleService
  ]
})
export class RoleModule {};