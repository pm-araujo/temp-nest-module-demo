import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { ORMService } from './orm.service';

@Module({
  imports: [ConfigModule],
  providers: [ORMService],
  exports: [ORMService],
})
export class ORMModule {}
