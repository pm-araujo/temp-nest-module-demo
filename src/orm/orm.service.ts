import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

const users = [
  { id: 1, name: 'User A', isActive: true, roles: [1] },
  { id: 2, name: 'User B', isActive: false, roles: [1, 2] },
  { id: 3, name: 'User C', isActive: false, roles: [] }
];

const roles = [
  { id: 1, name: 'Admin' },
  { id: 2, name: 'Management' }
];

@Injectable()
export class ORMService
  implements OnModuleInit {
  constructor(
      private readonly configService: ConfigService
  ) {
    console.log('instantiating ORM module')
  }
  async onModuleInit() {
    console.log('instantiating via onModuleInit ORM module')
  }

  getUsers() {
    return users;
  }
  
  getUser(userId: number) {
    const user = users.find(u => u.id == userId);

    if (!user) {
      throw new Error('ORM Error::User not found');
    }

    return user;
  }

  getRoles() {
    return roles;
  }

  getRole(roleId: number) {
    const role = roles.find(r => r.id == roleId);

    if (!role) {
      throw new Error('ORM Error::Role not found');
    }

    return role;
  }

  rolesByUser(userId: number) {
    const user = this.getUser(userId);

    const temp = roles.filter(r => user.roles.includes(r.id));

    return temp;
  }

  usersByRole(roleId: number) {
    const role = this.getRole(roleId);

    return users.filter(u => u.roles.includes(role.id));
  }
}
