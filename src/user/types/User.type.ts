import { Field, ObjectType, Int } from "@nestjs/graphql";
import { Role } from "src/role/types/Role.type";

@ObjectType()
export class User {
  @Field(type => Int)
  id: number;

  @Field()
  name: string;

  @Field()
  isActive: boolean;

  @Field(type => [Role], { nullable: true })
  roles?: Role[]
}
