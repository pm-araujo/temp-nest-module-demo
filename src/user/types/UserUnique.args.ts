import { Field, ArgsType, InputType, Int } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class UserUniqueArgs {
  @Field(type => Int)
  id: number;
}
