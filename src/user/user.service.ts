import { Injectable } from '@nestjs/common';
import {
  User,
  UserUniqueArgs
} from './types';

import UserNotFoundError from './errors/UserNotFound.error';
import { ORMService } from 'src/orm/orm.service';

@Injectable()
export class UserService {
  readonly className = this.constructor.name;

  constructor(
    private readonly ormService: ORMService
  ) {}

  async findOne(where: UserUniqueArgs): Promise<User> {

    try {
      const { roles, ...user } = this.ormService.getUser(where.id);

      return user;
    } catch(e) {
      throw new UserNotFoundError(this.className)
    }

  }

  async findAll(): Promise<User[]> {
    const users = this.ormService.getUsers();

    return users.map(({ roles, ...all}) => all);
  }
}