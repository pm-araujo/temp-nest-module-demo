import { Module } from '@nestjs/common';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';
import { RoleModule } from 'src/role/role.module';
import { RoleService } from 'src/role/role.service';
import { ORMModule } from 'src/orm/orm.module';

@Module({
  imports: [
    ORMModule,
    RoleModule
  ],
  providers: [
    UserResolver,
    UserService,
    RoleService
  ],
  exports: [
    UserService
  ]
})
export class UserModule {}
