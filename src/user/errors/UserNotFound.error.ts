import { MainAPIError } from "src/common/errors/MainAPIError.error";

class UserNotFoundError extends MainAPIError {
  constructor(errorContext: string) {
    const message = `User not found`;
    const code = 'UserNotFoundError';

    super(
      message,
      undefined, undefined, undefined, undefined, undefined,
      { code, errorContext }
    )
  }
}

export default UserNotFoundError;
