import { ResolveField, Parent, Resolver, Query, Args } from "@nestjs/graphql";

import { RoleService } from "src/role/role.service";
import { UserService } from "./user.service";

import { Role } from "src/role/types/Role.type";
import { User, UserUniqueArgs } from './types';

@Resolver(of => User)
export class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly roleService: RoleService
  ) {}


  @Query(returns => [User])
  users() {
    return this.userService.findAll();
  }

  @Query(returns => User)
  user(@Args('where') where: UserUniqueArgs) {
    return this.userService.findOne(where);
  }

  @ResolveField(returns => [Role])
  async roles(@Parent() user: User) {
    return this.roleService.rolesByUser(user.id);
  }
}