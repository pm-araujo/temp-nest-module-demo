import { GraphQLError } from "graphql";

/**
 * Wrapper error class for differentiating between
 *  system/framework errors and application errors that we define.
 */
export class MainAPIError extends GraphQLError {}
