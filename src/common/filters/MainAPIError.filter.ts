import { Catch, ArgumentsHost } from "@nestjs/common";
import { GqlExceptionFilter } from "@nestjs/graphql";
import { ConfigService } from "@nestjs/config";

import { LoggerService } from "src/logger/logger.service";

import { MainAPIError } from "../errors/MainAPIError.error";

/**
 * ExceptionFilter class for catching & logging
 *  application level errors
 *
 * A 'errorContext' property is expected to have been defined
 *  in the error.extensions object that communicates the origin
 *  of the error.
 */
@Catch(MainAPIError)
export class MainAPIErrorFilter implements GqlExceptionFilter {

  constructor(
    private readonly configService: ConfigService,
    private readonly logger: LoggerService
  ) {}

  catch(exception: MainAPIError, host: ArgumentsHost) {
    const { message, stack, extensions: { errorContext } } = exception;

    this.logger.error(message, stack, errorContext);

    return exception;
  }
}
