import { Injectable, Scope, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import chalk from 'chalk';

import * as dotenv from 'dotenv';

dotenv.config();

const {
  STAGE
} = process.env;

@Injectable()
export class LoggerService extends Logger implements OnModuleInit {
  private logger;

  constructor(
    // private readonly configService: ConfigService
  ) {
    super();
    console.log('instantiating new logger');
    // const stage = this.configService.get('STAGE');

    if (STAGE === 'test') {
      this.logger = {
        info(...args) {args},
        error(...args) {args},
        warn(...args) {args},
        debug(...args) {args}
      };
      return;
    } else {
      this.logger = {
        info(message: string, context?: string) {
          console.info(`${chalk.blue('INFO')} - [${chalk.gray(context)}] - ${message}`)
        },
        error(message: string, trace?: string, context?: string) {
          console.error(`${chalk.red('ERROR')} - [${chalk.gray(context)}] - ${message} - ${trace}`)
        },
        warn(message: string, context?: string) {
          console.warn(`${chalk.yellow('WARN')} - [${chalk.gray(context)}] - ${message}`)
        },
        debug(message: string, context?: string) {
          console.log(`${chalk.cyan('DEBUG')} - [${chalk.gray(context)}] - ${message}`)
        }
      }
    }
  }

  onModuleInit() {
    console.log('instantiating via onModuleInit logger service');
  }

  log(message: string, context = this.context) {
    this.logger.info(message, context);
  }

  error(message: string, trace?: string, context = this.context) {
    this.logger.error(message, trace, context);
  }

  warn(message: string, context = this.context) {
    this.logger.warn(message, context);
  }

  debug(message: string, context = this.context) {
    this.logger.debug(message, context);
  }

  verbose(message: string, context = this.context) {
    this.debug(message, context);
  }
}
