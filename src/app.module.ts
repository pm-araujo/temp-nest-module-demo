import { APP_FILTER } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { MainAPIErrorFilter } from './common/filters/MainAPIError.filter';
import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { LoggerModule } from './logger/logger.module';

@Module({
  providers: [
    {
      provide: APP_FILTER,
      useClass: MainAPIErrorFilter
    }
  ],
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    GraphQLModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const isProduction = configService.get<string>('STAGE') === 'prod';

        return {
          autoSchemaFile: true,
          debug: !isProduction,
          tracing: !isProduction,
          playground: !isProduction,
          context: ({ req }) => ({ req })
        };
      },
      inject: [ConfigService]
    }),
    UserModule,
    RoleModule,
    LoggerModule
  ]
})
export class AppModule {}
