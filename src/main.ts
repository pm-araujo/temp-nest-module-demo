import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './logger/logger.service';

async function bootstrap() {
  const logger = new LoggerService()
  const app = await NestFactory.create(AppModule, {
    logger
  });
  await app.listen(3000);
}
bootstrap();
